<?php

namespace Drupal\chemistry_footer\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provide config form to set content for local footer blocks.
 */
class SiteContactConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['chemistry_footer.site_contact'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'chemistry_footer_site_contact_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('chemistry_footer.site_contact');
    $site_config = $this->config('system.site');
    $base_url = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();

    $form['site_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contact email address for this site'),
      '#description' => $this->t('The email address that will appear in the site footer'),
      '#default_value' => $config->get('site_email'),
    ];

    $form['site_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site description for footer'),
      '#default_value' => $config->get('site_name'),
      '#description' => $this->t(
        'The text that appears above the contact email address. If not set, this will default to <em>Contact @site_name</em>',
        ['@site_name' => $site_config->get('name')]
      ),
      '#required' => FALSE,
    ];

    $form['contact_link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link to contacts page'),
      '#default_value' => $config->get('contact_link'),
      '#description' => $this->t(
        '(optional) a link to a relevant webpage, if appropriate. This needs to be a full URL, e.g. <em>@examplecontacts</em>',
        ['@examplecontacts' => $base_url . "contacts"]
      ),
      '#required' => FALSE,
    ];

    $form['contact_tel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contact telephone number for this site'),
      '#default_value' => $config->get('contact_tel'),
      '#description' => $this->t('If set, this telephone number will be added to the site footer.'),
      '#required' => FALSE,
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('chemistry_footer.site_contact')
      ->set('site_email', $form_state->getValue('site_email'))
      ->set('site_name', $form_state->getValue('site_name'))
      ->set('contact_link', $form_state->getValue('contact_link'))
      ->set('contact_tel', $form_state->getValue('contact_tel'))
      ->save();

    parent::submitForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $contact_link = $form_state->getValue('contact_link');
    if ($contact_link && !UrlHelper::isValid($contact_link, TRUE)) {
      $form_state->setErrorByName('contact_link', "$contact_link is not a valid link - must be a full URL, i.e. starting with http:// or https://");
    }
  }

}
