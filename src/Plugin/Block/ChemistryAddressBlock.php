<?php

namespace Drupal\chemistry_footer\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Chemistry Address block.
 *
 * @Block(
 *  id = "chemistry_contact",
 *  admin_label = "Chemistry contact details"
 * )
 */
class ChemistryAddressBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $content = [];

    $content = [
      '#type' => 'container',
      '#attached' => ['library' => ['chemistry_footer/chemistry_footer']],
    ];

    $content['dept_contact'] = [
      '#type' => 'container',
      '#attributes' => ['class' => 'chem-dept-contact'],
    ];

    $address[] = "Yusuf Hamied Department of Chemistry";
    $address[] = "Lensfield Road";
    $address[] = "Cambridge";
    $address[] = "CB2 1EW";
    $address[] = "T: +44 (0) 1223 336300";

    $content['dept_contact']['address-container'] = [
      '#type' => 'container',
      '#attributes' => ['class' => 'address-container'],
    ];

    foreach ($address as $line) {
      $content['dept_contact']['address-container'][] = [
        '#markup' => $line,
        '#suffix' => '<br />',
      ];
    }

    $content['dept_contact']['address-container'][] = [
      '#markup' => Link::fromTextAndUrl("enquiries@ch.cam.ac.uk", Url::fromUri("mailto:enquiries@ch.cam.ac.uk"))->toString(),
    ];

    $content['dept_contact']['info'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['info-container']],
    ];

    $content['dept_contact']['info-container'][] = [
      '#markup' => Link::fromTextAndUrl("Contacts", Url::fromUri("https://www.ch.cam.ac.uk/contacts"))->toString(),
      '#suffix' => '<br />',
    ];
    $content['dept_contact']['info-container'][] = [
      '#markup' => Link::fromTextAndUrl("Directions", Url::fromUri("https://www.ch.cam.ac.uk/directions-to-department"))->toString(),
      '#suffix' => '<br /><br />',
    ];

    $content['dept_contact']['info-container'][] = [
      '#markup' => Link::fromTextAndUrl("University Privacy Policy", Url::fromUri("http://www.cam.ac.uk/about-this-site/privacy-policy"))->toString(),
    ];

    return $content;
  }

}
