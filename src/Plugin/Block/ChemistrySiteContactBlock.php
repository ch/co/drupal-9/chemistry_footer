<?php

namespace Drupal\chemistry_footer\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Chemistry Site Contact block.
 *
 * @Block(
 *  id = "chemistry_site_contact",
 *  admin_label = "Chemistry site contacts"
 * )
 */
class ChemistrySiteContactBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Config factory.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
    $configuration,
    $plugin_id,
    $plugin_definition,
    $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $content = ['#type' => 'container'];

    $config = $this->configFactory->get('chemistry_footer.site_contact');
    $site_config = $this->configFactory->get('system.site');

    $our_cache_tags = $config->getCacheTags();

    $site_email = $config->get('site_email');
    $site_link = $config->get('contact_link');
    $site_telephone = $config->get('contact_tel');

    if ($site_email || $site_link || $site_telephone) {
      $content['site_contact'] = [
        '#type' => 'container',
        '#attributes' => [],
      ];

      $site_name = $config->get('site_name');

      $heading = "";
      if ($site_name) {
        $heading = $site_name;
      }
      else {
        $heading = "Contact " . $site_config->get('name');
        $our_cache_tags = Cache::mergeTags($our_cache_tags, $site_config->getCacheTags());
      }
      if ($site_link) {
        $heading = Link::fromTextAndUrl($heading, Url::fromUri($site_link))->toString();
      }

      $content['site_contact']['heading'] = [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => $heading,
        '#attributes' => ['id' => 'chemistry-footer4-header'],
      ];

      if ($site_email) {
        $content['site_contact']['email'] = [
          '#type' => 'markup',
          '#markup' => "<div class='pad-bottom'>Email: " . Link::fromTextAndUrl($site_email, Url::fromUri('mailto:' . $site_email))->toString() . "</div>",
        ];
      }

      if ($site_telephone) {
        $content['site_contact']['tel'] = [
          '#type' => 'markup',
          '#markup' => "<div>T: " . $site_telephone . "</div>",
        ];
      }
    }

    $parent_cache_tags = parent::getCacheContexts();

    $block = [
      'content' => $content,
      '#contextual_links' => [
        'chemistry_footer' => [
          'route_parameters' => [],
        ],
      ],
      'attributes' => [],
      '#cache' => [
        'tags' => Cache::mergeTags($our_cache_tags, $parent_cache_tags),
      ],
    ];

    return $block;
  }

}
