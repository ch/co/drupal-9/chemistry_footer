<?php

namespace Drupal\chemistry_footer\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Chemistry department links block.
 *
 * @Block(
 *  id = "chemistry_dept_links",
 *  admin_label = "Chemistry department links"
 * )
 */
class ChemistryDeptLinksBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $links = [];
    $links[] = Link::fromTextAndUrl('Study here', Url::fromUri('https://www.ch.cam.ac.uk/study'))->toString();
    $links[] = Link::fromTextAndUrl('Research', Url::fromUri('https://www.ch.cam.ac.uk/research'))->toString();
    $links[] = Link::fromTextAndUrl('News', Url::fromUri('https://www.ch.cam.ac.uk/news'))->toString();
    $links[] = Link::fromTextAndUrl('Jobs', Url::fromUri('https://www.ch.cam.ac.uk/jobs'))->toString();
    $links[] = Link::fromTextAndUrl('Talks', Url::fromUri('https://www.ch.cam.ac.uk/talks'))->toString();

    $block = [
      'content' => [
        '#type' => 'container',
        '#attributes' => ['class' => 'campl-navigation-list'],
        'foo' => [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => Link::fromTextAndUrl('About the Department', Url::fromUri('https://www.ch.cam.ac.uk/about'))->toString(),
          '#attributes' => ['id' => 'chemistry-footer2-header'],
        ],
        'links' => [
          '#theme' => 'item_list',
          '#type' => 'ul',
          '#attributes' => ['class' => 'campl-unstyled-list campl-page-children'],
          '#items' => $links,
        ],
      ],

    ];

    return $block;
  }

}
