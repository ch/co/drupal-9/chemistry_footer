<?php

namespace Drupal\chemistry_footer\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Chemistry dept resources block.
 *
 * @Block(
 *  id = "chemistry_resources",
 *  admin_label = "Chemistry resources"
 * )
 */
class ChemistryResourcesBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $links = [];
    $links[] = Link::fromTextAndUrl('Analytical Facilities', Url::fromUri('https://www.ch.cam.ac.uk/analytical-data-services'))->toString();
    $links[] = Link::fromTextAndUrl('Internal', Url::fromUri('https://www.ch.cam.ac.uk/intranet'))->toString();
    $links[] = Link::fromTextAndUrl('Library', Url::fromUri('https://www-library.ch.cam.ac.uk'))->toString();
    $links[] = Link::fromTextAndUrl('Photography and Reprographics', Url::fromUri('https://www.ch.cam.ac.uk/photography'))->toString();

    $block = [
      'content' => [
        '#type' => 'container',
        '#attributes' => ['class' => 'campl-navigation-list'],
        'foo' => [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => Link::fromTextAndUrl('Departmental Services', Url::fromUri('https://www.ch.cam.ac.uk/intranet'))->toString(),
          '#attributes' => ['id' => 'chemistry-footer3-header'],
        ],
        'links' => [
          '#theme' => 'item_list',
          '#type' => 'ul',
          '#attributes' => ['class' => 'campl-unstyled-list campl-page-children'],
          '#items' => $links,
        ],
      ],

    ];

    return $block;
  }

}
